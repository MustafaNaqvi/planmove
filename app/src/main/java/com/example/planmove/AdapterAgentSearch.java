package com.example.planmove;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AdapterAgentSearch extends RecyclerView.Adapter<AdapterAgentSearch.AgentSearchViewHolder> {

    private ArrayList<ItemAgentSearch> mItemList;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mListener = listener;
    }


    public AdapterAgentSearch(ArrayList<ItemAgentSearch> itemList) {
        mItemList = itemList;
    }

    @NonNull
    @Override
    public AgentSearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.agent_search_item, parent, false);
        AgentSearchViewHolder viewHolder = new AgentSearchViewHolder(view, mListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AgentSearchViewHolder holder, int position) {
        ItemAgentSearch currentItem = mItemList.get(position);
        holder.AgentLogo.setImageResource(currentItem.getmAgentLogo());
        holder.AgentName.setText(currentItem.getmAgentName());
        holder.AgentAddress.setText(currentItem.getmAgentAddress());
        holder.AgentCategory.setText(currentItem.getmAgentCategory());
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }

    public static class AgentSearchViewHolder extends RecyclerView.ViewHolder {

        public ImageView AgentLogo;
        public TextView AgentName;
        public TextView AgentAddress;
        public TextView AgentCategory;

        public AgentSearchViewHolder(@NonNull View itemView, final OnItemClickListener listener) {
            super(itemView);
            AgentLogo = itemView.findViewById(R.id.agentSearchItemLogo);
            AgentName = itemView.findViewById(R.id.agentSearchItemName);
            AgentAddress = itemView.findViewById(R.id.agentSearchItemAddress);
            AgentCategory = itemView.findViewById(R.id.agentSearchItemCategory);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            listener.onItemClick(position);
                        }
                    }
                }
            });
        }


    }
}

package com.example.planmove;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class AgentDetailsFragment extends Fragment {

    TextView agentNameText;
    TextView agentAddressText;
    Button aboutButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agent_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // This callback will only be called when MyFragment is at least Started.

        agentNameText = view.findViewById(R.id.agentName);
        agentNameText.setText(MainActivity.AgentName);

        agentAddressText = view.findViewById(R.id.agentAddress);

        String[] temp = MainActivity.AgentAddress.split(",");
        agentAddressText.setText("");
        for (int i = 0; i < temp.length; i++)
            temp[i] = temp[i].trim();
        for (int i = 0; i < temp.length; i++)
            agentAddressText.append(temp[i] + ",\n");

        aboutButton = view.findViewById(R.id.aboutAgentButton);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.AgentDescription = "Description";
                NavHostFragment.findNavController(AgentDetailsFragment.this)
                        .navigate(R.id.action_ThirdFragment_to_FourthFragment);
            }
        });



    }


}
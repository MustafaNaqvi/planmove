package com.example.planmove;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class AgentSearchFragment extends Fragment {

    private static final int AUTOCOMPLETE_REQUEST_CODE = 1;
    public String Api_key = "AIzaSyCsLa2_A1dfLF7b6Va6qWctbajz_T0UjAQ";

    Spinner radiusSpinner;
    Spinner agentTypeSpinner;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agent_search, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final List<String> radiusArray = new ArrayList<String>();
        radiusArray.add("This area only");
        radiusArray.add("Within 1/4 of a mile");
        radiusArray.add("Within 1/2 a mile");
        radiusArray.add("Within 1 mile");
        radiusArray.add("Within 3 miles");
        radiusArray.add("Within 5 miles");
        radiusArray.add("Within 10 miles");
        radiusArray.add("Within 15 miles");
        radiusArray.add("Within 20 miles");
        radiusArray.add("Within 30 miles");
        radiusArray.add("Within 40 miles");

        final List<String> agentTypeArray = new ArrayList<String>();
        agentTypeArray.add("Sales and Lettings");
        agentTypeArray.add("Sales");
        agentTypeArray.add("Lettings");

        ArrayAdapter<String> radiusAdapter = new ArrayAdapter<String>(
                view.getContext(), android.R.layout.simple_spinner_item, radiusArray);

        radiusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        radiusSpinner = (Spinner) view.findViewById(R.id.radius_spinner);
        radiusSpinner.setAdapter(radiusAdapter);

        radiusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                MainActivity.Radius = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                MainActivity.Radius = "This area only";
            }
        });

        ArrayAdapter<String> agentTypeAdapter = new ArrayAdapter<String>(
                view.getContext(), android.R.layout.simple_spinner_item, agentTypeArray);

        agentTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        agentTypeSpinner = (Spinner) view.findViewById(R.id.agentType_spinner);
        agentTypeSpinner.setAdapter(agentTypeAdapter);

        agentTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                MainActivity.AgentType = adapterView.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                MainActivity.AgentType = "Sales and Lettings";
            }
        });


        /**
         Initialize Places. For simplicity, the API key is hard-coded. In a production
         environment we recommend using a secure mechanism to manage API keys.
         **/
//        if (!Places.isInitialized()) {
//            Places.initialize(getApplicationContext(), Api_key);
//        }
//
//         Set the fields to specify which types of place data to
//         return after the user has made a selection.
//        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME);
////
////        // Start the autocomplete intent.
//        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
//                .build(this);
//        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
//        PlacesClient placesClient = Places.createClient(getApplicationContext());

        searchAgent(view);
    }

    public void searchAgent(View view) {
        view.findViewById(R.id.agentSearchButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(AgentSearchFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
//                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
                Toast toast = Toast.makeText(getContext(), place.getName() + "\t" + place.getAddress() + "\t" + place.getId(), Toast.LENGTH_LONG);
                toast.show();
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
//                Log.i(TAG, status.getStatusMessage());
                Toast toast = Toast.makeText(getContext(), status.getStatusMessage(), Toast.LENGTH_LONG);
                toast.show();
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
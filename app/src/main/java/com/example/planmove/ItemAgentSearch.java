package com.example.planmove;

public class ItemAgentSearch {

    private int mAgentLogo;
    private String mAgentName;
    private String mAgentAddress;
    private String mAgentCategory;

    public ItemAgentSearch(int mAgentLogo, String mAgentName, String mAgentAddress, String mAgentCategory) {
        this.mAgentLogo = mAgentLogo;
        this.mAgentName = mAgentName;
        this.mAgentAddress = mAgentAddress;
        this.mAgentCategory = mAgentCategory;
    }

    public int getmAgentLogo() {
        return mAgentLogo;
    }

    public void setmAgentLogo(int mAgentLogo) {
        this.mAgentLogo = mAgentLogo;
    }

    public String getmAgentName() {
        return mAgentName;
    }

    public void setmAgentName(String mAgentName) {
        this.mAgentName = mAgentName;
    }

    public String getmAgentAddress() {
        return mAgentAddress;
    }

    public void setmAgentAddress(String mAgentAddress) {
        this.mAgentAddress = mAgentAddress;
    }

    public String getmAgentCategory() {
        return mAgentCategory;
    }

    public void setmAgentCategory(String mAgentCategory) {
        this.mAgentCategory = mAgentCategory;
    }
}

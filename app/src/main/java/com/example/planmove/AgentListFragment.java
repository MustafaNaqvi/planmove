package com.example.planmove;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class AgentListFragment extends Fragment {

    ArrayList<ItemAgentSearch> itemList;
    private RecyclerView mRecyclerView;
    private AdapterAgentSearch mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    TextView locationInfoTextView;
    TextView categoryTextView;
    TextView agentCountTextView;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agent_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // This callback will only be called when MyFragment is at least Started.




        locationInfoTextView = view.findViewById(R.id.locationInfoTextView);
        locationInfoTextView.setText(MainActivity.Location);
        categoryTextView = view.findViewById(R.id.categoryTextView);
        categoryTextView.setText(MainActivity.AgentType);

        createItemList();
        buildRecyclerView(view);

        agentCountTextView = view.findViewById(R.id.agentCountTextView);
        agentCountTextView.setText(String.format("%d of %d", mAdapter.getItemCount(), mAdapter.getItemCount()));
/*        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(AgentListFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });*/
    }

    public void createItemList() {
        itemList = new ArrayList<ItemAgentSearch>();
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name1", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name2", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name3", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name4", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name5", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name6", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name7", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name8", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name9", "Address, Address, Address", "Category"));
        itemList.add(new ItemAgentSearch(R.drawable.ic_baseline_accessibility, "Agent Name10", "Address, Address, Address", "Category"));
    }

    public void buildRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.agentsRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getContext());
        mAdapter = new AdapterAgentSearch(itemList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new AdapterAgentSearch.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                MainActivity.AgentName = itemList.get(position).getmAgentName();
                MainActivity.AgentAddress = itemList.get(position).getmAgentAddress();
                NavHostFragment.findNavController(AgentListFragment.this)
                        .navigate(R.id.action_SecondFragment_to_thirdFragment);
            }
        });

    }
}